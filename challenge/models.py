# -*- coding: utf-8 -*-
# from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.db import models
from datetime import datetime, timedelta

from . import shortuuid


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    userID = models.IntegerField()
    
    def __str__(self):
        return self.user.username


class Challenge(models.Model):
    title = models.CharField(max_length=50, blank=False, null=False)
    description = models.TextField()
    owner = models.ForeignKey(UserProfile)
    start_datetime = models.DateTimeField()
    end_datetime = models.DateTimeField()
    when_created = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return self.title


class Entry(models.Model):
    challenge = models.ForeignKey(Challenge, null=False)
    title = models.CharField(max_length=50, blank=False, null=False)
    owner = models.ForeignKey(UserProfile, null=False)
    when_created = models.DateTimeField(default=datetime.now)
    # video

    def __str__(self):
        return self.title

    def votes(self):
        return self.vote.count()


class Vote(models.Model):
    user = models.ForeignKey(User, null=False)
    entry = models.ForeignKey(Entry, null=False)
    when_created = models.DateTimeField(default=datetime.now)


def generate_code():
    return shortuuid.uuid()

class InviteCode(models.Model):
    code = models.CharField(max_length=20,default=generate_code)
    challenge = models.ForeignKey(Challenge)
    used = models.BooleanField()