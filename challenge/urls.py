from django.conf.urls import url, include
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name="index"),
    url(r'^home/$', views.HomeView.as_view(), name="home"),
    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^logout/$', views.Logout.as_view(), name='logout'),


    url(r'^trending/$', views.TrendingView.as_view(), name="trending"),
    url(r'^done/$', views.DoneView.as_view(), name="done"),

]
