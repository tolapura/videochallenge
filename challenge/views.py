# -*- coding: utf-8 -*-
# from __future__ import unicode_literals
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required

from .models import UserProfile


class IndexView(View):

    def get(self, request):
        if request.user.is_authenticated:
            return redirect('challenge:home')
        else:
            return redirect('challenge:login')


class LoginView(View):

    def get(self, request):
        return render(request, 'challenge/login.html')


class Logout(View):
    # @login_required
    def get(self, request):
        logout(request)
        return redirect('challenge:index')


class HomeView(View):
    # @login_required
    def get(self, request):
        if not request.user.is_authenticated:
            return redirect('challenge:login')

        fb_user = request.user
        try:
            facebook_login = fb_user.social_auth.get(provider='facebook')
        except:
            return redirect('challenge:index')

        fb_id = facebook_login.extra_data.get('id')

        try:
          user = UserProfile.objects.get(userID=fb_id)
        except UserProfile.DoesNotExist:
          user = UserProfile(user=request.user,userID=fb_id)
          user.save()

        return render(request, 'challenge/home.html')


class TrendingView(View):

    def get(self, request):
        if not request.user.is_authenticated:
            return redirect('challenge:login')

        return render(request, 'challenge/trending.html')


class DoneView(View):

    def get(self, request):
        if not request.user.is_authenticated:
            return redirect('challenge:login')

        return render(request, 'challenge/done.html')
